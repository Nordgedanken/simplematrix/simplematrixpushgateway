package main

import (
	"encoding/json"
	"firebase.google.com/go"
	"firebase.google.com/go/messaging"
	"fmt"
	"github.com/gorilla/mux"
	"golang.org/x/net/context"
	"google.golang.org/api/option"
	"time"

	"log"
	"net/http"
)

var FCMClient *messaging.Client

func main() {
	// Create a FCM client to send the message.
	err := googleLogin()
	if err != nil {
		log.Println(fmt.Sprintf("[ERR] %s", err))
		panic(1)
	}
	r := mux.NewRouter()
	r.HandleFunc("/_matrix/push/v1/notify", NotifyHandler).Methods("POST")
	srv := &http.Server{
		Handler: r,
		Addr:    "127.0.0.1:8888",
		// Good practice: enforce timeouts for servers you create!
		WriteTimeout: 15 * time.Second,
		ReadTimeout:  15 * time.Second,
	}

	log.Fatal(srv.ListenAndServe())
}

func googleLogin() error {
	opt := option.WithCredentialsFile("./serviceAccountKey.json")
	app, err := firebase.NewApp(context.Background(), nil, opt)
	if err != nil {
		return fmt.Errorf("error initializing app: %v", err)
	}
	FCMClient, err = app.Messaging(context.Background())
	if err != nil {
		return err
	}
	return nil
}

func NotifyHandler(w http.ResponseWriter, r *http.Request) {
	/*bodyBytes, err := ioutil.ReadAll(r.Body)
	if err != nil {
		log.Println(fmt.Sprintf("[ERR] %s", err))
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	bodyString := string(bodyBytes)
	log.Println(fmt.Sprintf("[DEBUG] %s", bodyString))*/
	decoder := json.NewDecoder(r.Body)
	var t NotifyJson
	err := decoder.Decode(&t)
	if err != nil {
		log.Println(fmt.Sprintf("[ERR] %s", err))
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	var errored []string
	for _, device := range t.Notification.Devices {
		err = sendToGCM(device.Pushkey, &t)
		if err != nil {
			log.Println(fmt.Sprintf("[ERR] %s", err))
			errored = append(errored, device.Pushkey)
		}
	}

	var errResponse ErrResponse
	errResponse.Rejected = errored
	data, err := json.Marshal(errResponse)
	if err != nil {
		log.Println(fmt.Sprintf("[ERR] %s", err))
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	_, err = w.Write(data)
	if err != nil {
		log.Println(fmt.Sprintf("[ERR] %s", err))
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
}

func sendToGCM(pushkey string, data *NotifyJson) error {
	// Create the message to be sent.
	msg := &messaging.Message{
		Token: pushkey,
		Data: map[string]string{
			"id":      data.Notification.EventID,
			"room_id": data.Notification.RoomID,
			"sender":  data.Notification.Sender,
			"msgtype": data.Notification.Content.Msgtype,
			"body":    data.Notification.Content.Body,
		},
	}

	if data.Notification.Prio == "low" {
		msg.Data["priority"] = "normal"
	} else {
		msg.Data["priority"] = "high"
	}

	// Send the message and receive the response without retries.
	response, err := FCMClient.Send(context.Background(), msg)
	if err != nil {
		return err
	}
	log.Println(response)

	return nil
}

type ErrResponse struct {
	Rejected []string `json:"rejected"`
}

type NotifyJson struct {
	Notification struct {
		EventID           string `json:"event_id"`
		RoomID            string `json:"room_id"`
		Type              string `json:"type"`
		Sender            string `json:"sender"`
		SenderDisplayName string `json:"sender_display_name"`
		RoomName          string `json:"room_name"`
		RoomAlias         string `json:"room_alias"`
		Prio              string `json:"prio"`
		Content           struct {
			Msgtype string `json:"msgtype"`
			Body    string `json:"body"`
		} `json:"content"`
		Counts struct {
			Unread      int `json:"unread"`
			MissedCalls int `json:"missed_calls"`
		} `json:"counts"`
		Devices []struct {
			AppID     string `json:"app_id"`
			Pushkey   string `json:"pushkey"`
			PushkeyTs int    `json:"pushkey_ts"`
			Data      struct {
				Format string `json:"format"`
			} `json:"data"`
			Tweaks struct {
				Sound string `json:"sound"`
			} `json:"tweaks"`
		} `json:"devices"`
	} `json:"notification"`
}
